############################
Sekilas tentang project ini
############################
Project ini tentang Aplikasi Penggajian Karyawan yang dibangun menggunakan CodeIgniter 3, Cara kerja aplikasi ini diatur berdasarkan Jumlah Absensi/Kehadiran dan Jabatan, segala Issue yang ada dalam Project ini silahkan laporkan, Kami/Pemilik Project ini sangat menerima segala Issue yang ada pada Project ini.

###################
Apa Itu CodeIgniter
###################

CodeIgniter adalah Framework PHP yang menggunakan konsep MVC (Model-View-Controller), konsep ini memudahkan Pengembang Web untuk Mengembangkan Project yang dikerjakan, karena akan lebih terstruktur dan rapi. Saat ini CodeIgniter sudah mencapai versi 4

*******************
System Requirement
*******************

Disarankan untuk menggunakan PHP versi 5.6 atau yang lebih baru.

CodeIgniter 3 seharusnya dapat dijalankan pada PHP versi 5.3.7, namun itu tidak disarankan karena alasan keamanan dan pengembangan yang sudah usang, dan kemungkinan beberapa fitur tidak akan berjalan dengan baik jika menggunakan PHP versi 5.3.7 atau kebawahnya.

*******
Lisensi
*******

Untuk Lisensi CodeIgniter 3 dapat dilihat pada link berikut.
<https://github.com/bcit-ci/CodeIgniter/blob/develop/user_guide_src/source/license.rst>`_.

***************
Pengakuan
***************

Tim CodeIgniter menyatakan Terima Kasih kepada EllisLab, semua
Kontributor ke pengguna CodeIgniter dan Anda.
